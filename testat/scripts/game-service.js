export const DELAY_MS = 1000;

export const rankings = {
    Markus: {user: 'Markus', win: 3, lost: 6},
    Michael: {user: 'Michael', win: 4, lost: 5},
    Lisa: {user: 'Lisa', win: 5, lost: 5},
    Laura: {user: 'Laura', win: 14, lost: 5},
    Kaspar: {user: 'Kaspar', win: 14, lost: 5},
};

export const HANDS = ['Schere', 'Stein', 'Papier', 'Brunnen', 'Sreichholz'];

export const RESULTMAPPING = [
    [false, true, false, false, true],
    [false, false, true, false, true],
    [true, false, false, true, false],
    [true, true, false, false, false],
    [false, false, true, true, false],
];

let isConnectedState = false;

export function setConnected(newIsConnected) {
    isConnectedState = Boolean(newIsConnected);
}

export function isConnected() {
    return isConnectedState;
}

function getRankingFromServer(json, rankingsCallbackHandlerFn) {
    const items = Object.values(json);
    const sortedRanking = Object.values(items);
    sortedRanking.sort((a, b) => b.win - a.win);
    rankingsCallbackHandlerFn(sortedRanking);
}

export function getRankings(rankingsCallbackHandlerFn) {
    if (isConnectedState) {
        /* Place to make call to the server */
        fetch('https://stone.dev.ifs.hsr.ch/ranking')
            .then((r) => r.json())
            .then((json) => getRankingFromServer(json, rankingsCallbackHandlerFn));
    } else {
        const sortedRanking = Object.values(rankings);
        sortedRanking.sort((a, b) => b.win - a.win);
        setTimeout(() => rankingsCallbackHandlerFn(sortedRanking), DELAY_MS);
    }
}

export function evaluateHand(playerName, playerHand, callBackHandler) {
    const translationArr = ['Schere', 'Stein', 'Papier', 'Brunnen', 'Streichholz'];

    if (isConnected()) {
        fetch(`https://stone.dev.ifs.hsr.ch/play?playerName=${playerName}&playerHand=${translationArr[playerHand]}`)
            .then((response) => response.json())
            .then((res) => {
                callBackHandler(res.win, playerHand, res.choice);
        });
    } else {
        const computerChoice = Math.floor(Math.random() * 5);
        const resultMapping = RESULTMAPPING[computerChoice][playerHand];
        callBackHandler(resultMapping, playerHand, translationArr[computerChoice]);
    }
}
