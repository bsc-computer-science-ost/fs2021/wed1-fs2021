/*
    OST - Marco Agostini
    WED1 Testat - FS2021
    Version: 1.2
 */

import * as gs from './game-service.js';
import {evaluateHand} from './game-service.js';

const mainView = document.querySelector('#main');
const gameView = document.querySelector('#game');
const playerName = document.querySelector('#playername');
const playerInput = document.querySelector('#name');
const gameResult = document.querySelector('#result');
const timerValue = document.querySelector('#timer');
const rankingList = document.querySelector('#ranking');
const buttons = document.querySelectorAll('button.options');
const errorMessage = document.querySelector('.name-error');

const changeToGame = document.querySelector('#toGame');
const changeToMenue = document.querySelector('#toMenue');
const changeToServer = document.querySelector('#toServer');
const resultTable = document.querySelector('#results');
const eventButton = document.querySelector('#event');
let buttonsEnabled = true;

function disableButtons() {
    buttonsEnabled = !buttonsEnabled;
    buttons.forEach((b) => {
       b.setAttribute('disabled', true);
    });
}

function enableButtons() {
    buttons.forEach((b) => {
        b.removeAttribute('disabled');
    });
    buttonsEnabled = !buttonsEnabled;
}

function updateRanking(sortedRanking) {
    rankingList.innerHTML = '';
    let rank = 1;
    for (let i = 0; i < 10 && i < sortedRanking.length; i++) {
        let rankText = 'Error';
        if ((i + 1 < sortedRanking.length) && sortedRanking[i].win === sortedRanking[i + 1].win) {
            rankText = `${rank}. mit ${sortedRanking[i].win} Siegen sind ${sortedRanking[i].user} und ${sortedRanking[i + 1].user}`;
            i++;
        } else {
            rankText = `${rank}. mit ${sortedRanking[i].win} Siegen ist ${sortedRanking[i].user}`;
        }
        const entry = `<li> ${rankText} </li>`;
        rankingList.insertAdjacentHTML('beforeend', entry);
        rank++;
    }
}

function renderRanking() {
    rankingList.innerHTML = 'Loading....';
    gs.getRankings(updateRanking);
}

function changeView() {
    if (playerInput.value.length < 3) {
        errorMessage.hidden = false;
        return;
    }

    errorMessage.hidden = true;
    mainView.hidden = !mainView.hidden;
    gameView.hidden = !gameView.hidden;
    playerName.innerHTML = `${playerInput.value} wähle deine Hand!`;
    renderRanking();
}

function initView() {
    gameView.hidden = 'true';
    renderRanking();
}

function changeService() {
    gs.setConnected(!gs.isConnected());
    renderRanking();
}

function startTimer() {
    let timeLeft = 3;
    timerValue.innerHTML = `Nächste Runde in ${timeLeft}`;

    const downloadTimer = setInterval(() => {
        timeLeft--;
        timerValue.innerHTML = `Nächste Runde in ${timeLeft}`;
        if (timeLeft === 0) {
            timerValue.textContent = 'VS';
            clearInterval(downloadTimer);
            enableButtons();
        }
    }, gs.DELAY_MS);
}

function printResult(result, playerHand, computerHand) {
    const resultRow = resultTable.insertRow(1);
    const gameResultCell = resultRow.insertCell(0);
    const playerPickCell = resultRow.insertCell(1);
    const computerPickCell = resultRow.insertCell(2);
    let userPushed = false;

    if (result) {
        gameResultCell.innerHTML = '✔';
        gameResult.innerHTML = 'Gratuliere, du hast gewonnen!';
    } else {
        gameResultCell.innerHTML = '✖';
        gameResult.innerHTML = 'Du hast leider nicht gewonnen...';
    }
    playerPickCell.innerHTML = `${gs.HANDS[playerHand]}`;
    computerPickCell.innerHTML = computerHand;

    if (playerInput.value in gs.rankings) {
        if (result === true) {
            gs.rankings[playerInput.value].win++;
        } else {
            gs.rankings[playerInput.value].lost++;
        }
        userPushed = true;
    }

    if (!userPushed) {
        if (result) {
            gs.rankings[playerInput.value] = {user: playerInput.value, win: 1, lost: 0};
        } else {
            gs.rankings[playerInput.value] = {user: playerInput.value, win: 0, lost: 1};
        }
    }
}

function newRound(event) {
    if (!(event.target.value === undefined)) {
        const playerHand = event.target.value;
        disableButtons();
        evaluateHand(playerName, playerHand, printResult);
        startTimer();
    }
}

/* Event Listener */
changeToGame.addEventListener('click', changeView);
changeToMenue.addEventListener('click', changeView);
changeToServer.addEventListener('click', changeService);
eventButton.addEventListener('click', (event) => newRound(event));
initView();
