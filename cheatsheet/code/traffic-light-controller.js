// Controller - File
import TrafficLightModel from './traffic-light-model.js';
import TrafficLightView from './traffic-light-view.js';
const LIGHTS_COUNT = 3;
const trafficLightModel = new TrafficLightModel(LIGHTS_COUNT);
const trafficLightView = new TrafficLightView(trafficLightModel);
function switchLights() {
  trafficLightModel.goToNextLightState();
  trafficLightView.updateView();
}
trafficLightView.registerSwitchHandler(switchLights);
trafficLightView.updateView();
