// View - File
export default class TrafficLightView {
  constructor(trafficLightModel) {
    this.tafficLightModel = trafficLightModel;
    this.lightElements = document.querySelectorAll('.light');
    this.switchButton = document.getElementById('switch');
  }
  updateView() {
    this.lightElements
      .forEach((lightElement) => (
        lightElement.classList.add('off')));
    this.lightElements[this.tafficLightModel.lightState]
      .classList.remove('off');
  }
  registerSwitchHandler(handlerFn) {
    this.switchButton.addEventListener('click', handlerFn);
  }
}
