// Fetch Data from a Server -> returns a promise
fetch('https://stone.dev.ifs.hsr.ch/ranking')
  .then((response) => response.json())
  .then((res) => {
    TestCallbackFn(res);
  })
  .catch((error) => {
    console.log('Error:', error);
});
//The same in a function with await / Await must be in async function
async function getData(url) {
  try {
  let path = '/ranking'
  let data = await fetch(`url${path}`);
  TestCallbackFn;
  } catch {
    console.log('Error;', data.error);
  }
}
