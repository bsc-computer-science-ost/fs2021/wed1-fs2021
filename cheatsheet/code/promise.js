var hans = new Promise(function(resolve, reject) {
    let x = "Hello";
    let y = "World";
    if(x === y) {
        resolve("x=y");
    } else {
        reject("x!=y");
    } });
hans.then(function (successMsg) {
    console.log(successMsg); //Output: x=y
}).catch(function (errorMsg) {
    console.log(errorMsg); //Output: x!=y
});