// Simple Counter Function
function makeCounterFn(start){
    return () => {return start++;};
}
const fn = makeCounterFn(10);
console.log(fn()); //10
console.log(fn()); //11
console.log(fn()); //12