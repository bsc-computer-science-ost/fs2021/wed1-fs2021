// Simple timeout with setIntervall
let timeLeft = 3;
const downloadTimer = setInterval(() => {
    timeLeft--;
    timerValue.innerHTML = `Naechste Runde in ${timeLeft}`;
    if (timeLeft === 0) {
        clearInterval(downloadTimer);
        enableButtons();
        }
    }, DELAY_MS);
}