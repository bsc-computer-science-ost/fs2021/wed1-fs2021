// Model - File
export default class TrafficLightModel {
  constructor(lightsCount = 3) {
    this.lightsCount = lightsCount;
    this.lightState = 0;
  }
  goToNextLightState() {
    this.lightState = (
      (this.lightState + 1) % this.lightsCount);
  }
}
